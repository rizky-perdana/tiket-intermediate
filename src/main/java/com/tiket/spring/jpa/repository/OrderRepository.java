package com.tiket.spring.jpa.repository;

import com.tiket.spring.entity.Orders;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Orders, Long>, PagingAndSortingRepository<Orders, Long> {
	
	@Query(value = "select "
		 	+" order.\"OrderID\" as orderID, "
			+"	 product.\"ProductName\" as prodName, "
			+"	 odetail.\"Quantity\" as quantity, "
			+"	 odetail.\"UnitPrice\" as hargaSatuan, "
			+"	 odetail.\"Discount\" as diskon, "
			+"	 CASE "
			+"	 	WHEN odetail.\"Discount\">0 THEN (odetail.\"UnitPrice\"* odetail.\"Quantity\")-((odetail.\"UnitPrice\"* odetail.\"Quantity\"*odetail.\"Discount\")/100) "
			+"	 	ELSE (odetail.\"UnitPrice\"* odetail.\"Quantity\") "
			+"	 	END "
			+"	 	AS subTotal "
			+"	 from public.\"Orders\" order " 
			+"	 inner join public.\"Order_Details\" od on odetail.\"OrderID\" = order.\"OrderID\" "
			+"	 inner join public.\"Products\" product on product.\"ProductID\" = odetail.\"ProductID\" "
			+"	 where order.\"OrderID\" = :orderID" , nativeQuery = true)
	public List<ProductDetail> findProductDetail(@Param("orderID") Long orderID);
	
	 public static interface ProductDetail {
		 Long getOrderID();
	     String getProdName();
	     Long getQuantity();
	     Double getHargaSatuan();
	     Long getDiscount();
	     Double getSubTotal();
	  }
}
