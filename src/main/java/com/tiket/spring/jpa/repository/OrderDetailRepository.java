package com.tiket.spring.jpa.repository;

import com.tiket.spring.entity.OrderDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetails, Long>, PagingAndSortingRepository<OrderDetails, Long> {

}
