package com.tiket.spring.jpa.repository;

import com.tiket.spring.entity.Employees;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface EmployeeRepository extends JpaRepository<Employees,Long>, PagingAndSortingRepository<Employees, Long> {

}
