package com.tiket.spring.jpa.repository;

import com.tiket.spring.entity.Products;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Products, Long>, PagingAndSortingRepository<Products, Long> {

}
