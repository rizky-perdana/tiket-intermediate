package com.tiket.spring.jpa.repository;

import com.tiket.spring.entity.ShippingMethods;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShippingMethodRepository extends JpaRepository<ShippingMethods, Long>, PagingAndSortingRepository<ShippingMethods, Long> {

}
