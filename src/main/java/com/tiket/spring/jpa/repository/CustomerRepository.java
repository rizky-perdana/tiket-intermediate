package com.tiket.spring.jpa.repository;

import com.tiket.spring.entity.Customers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CustomerRepository extends JpaRepository<Customers, Long>, PagingAndSortingRepository<Customers, Long>  {
}
