package com.tiket.spring.service;

import com.tiket.spring.entity.Customers;
import java.util.List;

public interface CustomerService {
	public void uploadCustomer(List<Customers> customers);
	public List<Customers> findCustomers();
	public Customers findCustomer(Long customerID);
}
