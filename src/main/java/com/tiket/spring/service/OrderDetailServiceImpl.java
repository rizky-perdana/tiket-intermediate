package com.tiket.spring.service;

import com.tiket.spring.entity.OrderDetails;
import com.tiket.spring.jpa.repository.OrderDetailRepository;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderDetailServiceImpl implements OrderDetailService {
	
	@Autowired
	private OrderDetailRepository orderDetailRepository;

	@Override
	public void uploadOrderDetail(List<OrderDetails> ods) {
		for (OrderDetails orderDetails : ods) {
			orderDetailRepository.save(orderDetails);
		}
		
	}

}
