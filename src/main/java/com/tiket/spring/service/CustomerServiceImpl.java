package com.tiket.spring.service;

import com.tiket.spring.entity.Customers;
import com.tiket.spring.jpa.repository.CustomerRepository;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CustomerServiceImpl implements CustomerService{

	@Autowired
	private CustomerRepository customerRepository;
	
	@Override
	public void uploadCustomer(List<Customers> customers) {
		for (Customers customer : customers) {
			customerRepository.save(customer);
		}		
	}
	
	@Override
	public List<Customers> findCustomers(){
		return customerRepository.findAll();
	}

	@Override
	public Customers findCustomer(Long customerID) {
		Customers customer = customerRepository.findById(customerID).get();
		return customer;
	}

}
