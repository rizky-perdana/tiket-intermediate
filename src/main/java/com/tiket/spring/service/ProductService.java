package com.tiket.spring.service;

import com.tiket.spring.entity.Products;
import java.util.List;

public interface ProductService {
	public void uploadProducts(List<Products> products);
	public Products findProduct(Long productID);
	public List<Object> findProductByOrder();
}
