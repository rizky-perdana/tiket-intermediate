package com.tiket.spring.service;

import com.tiket.spring.entity.Employees;
import java.util.List;

public interface EmployeeService {
	public void uploadEmployee(List<Employees> emp);
	public Employees findEmployee(Long employeeID);
	public List<Employees> findEmployees();
	public List<Object> findEmployeeByOrder();
}
