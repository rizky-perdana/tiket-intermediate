package com.tiket.spring.service;

import com.tiket.spring.entity.ShippingMethods;
import java.util.List;

public interface ShippingMethodService {
	public void uploadShippingMethod(List<ShippingMethods> sm);
	public ShippingMethods findSM(Long smID);
	public List<Object> findShippingMethodByOrder();
}
