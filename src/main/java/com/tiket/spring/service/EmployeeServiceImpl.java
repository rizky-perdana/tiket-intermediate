package com.tiket.spring.service;

import com.tiket.spring.entity.Employees;
import com.tiket.spring.entity.Orders;
import com.tiket.spring.jpa.repository.EmployeeRepository;
import com.tiket.spring.jpa.repository.OrderRepository;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	OrderRepository orderRepository;

	@Override
	public void uploadEmployee(List<Employees> emp) {
	for (Employees employees : emp) {
		employeeRepository.save(employees);
	}
	}

	@Override
	public Employees findEmployee(Long employeeID) {
		Employees emp = employeeRepository.findById(employeeID).get();
		return emp;
	}

	@Override
	public List<Employees> findEmployees() {
		return employeeRepository.findAll();
	}

	@Override
	public List<Object> findEmployeeByOrder() {
		List<Object> listEmp = new ArrayList();
		
		List<Orders> orders = orderRepository.findAll();
		for (Orders order : orders) {
			Map<String, Object> map = new HashMap();
			Employees emp = order.getEmployee();
			map.put("EmployeeID", emp.getEmployeeID());
			map.put("FirstName", emp.getFirstName());
			map.put("LastName", emp.getLastName());
			map.put("Title", emp.getTitle());
			map.put("WorkPhone", emp.getWorkPhone());
			map.put("OrderID", order.getOrderID());
			listEmp.add(map);	
		}
		
		
		return listEmp;
	}

}
