package com.tiket.spring.service;

import com.tiket.spring.entity.Orders;
import com.tiket.spring.entity.ShippingMethods;
import com.tiket.spring.jpa.repository.OrderRepository;
import com.tiket.spring.jpa.repository.ShippingMethodRepository;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShippingMethodServiceImpl implements ShippingMethodService {
	@Autowired
	private ShippingMethodRepository smRepo;
	
	@Autowired
	private OrderRepository orderRepo;
	
	@Override
	public void uploadShippingMethod(List<ShippingMethods> sm) {
            sm.forEach((shippingMethods) -> {
                smRepo.save(shippingMethods);
            });
		
	}

	@Override
	public ShippingMethods findSM(Long smID) {
		ShippingMethods sm = smRepo.findById(smID).get();
		return sm;
	}

	@Override
	public List<Object> findShippingMethodByOrder() {
		List<Object> listSm = new ArrayList<>();
		List<Orders> orders = orderRepo.findAll();
                orders.stream().map((order) -> {
                    Map<String, Object> map = new HashMap<>();
                    ShippingMethods sm = order.getShippingMethods();
                    map.put("OrderID", order.getOrderID());
                    map.put("ShippingMethodID", sm.getShippingMethodID());
                    map.put("ShippingMethod", sm.getShippingMethod());
                return map;
            }).forEachOrdered((map) -> {
                listSm.add(map);
            });
		return listSm;
	}

}
