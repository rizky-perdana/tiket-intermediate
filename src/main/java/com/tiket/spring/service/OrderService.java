package com.tiket.spring.service;

import com.tiket.spring.entity.Orders;
import com.tiket.spring.jpa.repository.OrderRepository.ProductDetail;
import java.util.List;

public interface OrderService {
	public void uploadOrder(List<Orders> orders);
	public Orders findOrder(Long orderID);
	public List<ProductDetail> findProductDetail(Long orderID);
	public List<Object> payDetail(Long orderID);
}
