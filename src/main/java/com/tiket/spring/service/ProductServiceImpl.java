package com.tiket.spring.service;

import com.tiket.spring.entity.OrderDetails;
import com.tiket.spring.entity.Orders;
import com.tiket.spring.entity.Products;
import com.tiket.spring.jpa.repository.OrderDetailRepository;
import com.tiket.spring.jpa.repository.ProductRepository;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductRepository productRepo;
	
	@Autowired
	private OrderDetailRepository odRepo;

	@Override
	public void uploadProducts(List<Products> products) {
		for (Products product : products) {
			productRepo.save(product);
		}	
	}

	@Override
	public Products findProduct(Long productID) {
		Products prod = productRepo.findById(productID).get();
		return prod;
	}

	@Override
	public List<Object> findProductByOrder() {
		List<Object> listProd = new ArrayList<>();
		List<OrderDetails> orderDetail = odRepo.findAll();
		for (OrderDetails od : orderDetail) {
			Map<String, Object> map = new HashMap<>();
			Products prod = od.getProduct();
			Orders order = od.getOrder();
			map.put("OrderID", order.getOrderID());
			map.put("OrderDetailID", od.getOrderDetailID());
			map.put("ProductName", prod.getProductName());
			map.put("Quantity", od.getQuantity());
			map.put("UnitPrice", "$"+od.getUnitPrice());
			map.put("Discount", "$"+od.getDiscount());
			Long disc = od.getDiscount();
			if(disc != 0){
				Double subTotal = od.getUnitPrice() * disc / 100;
			map.put("SubTotal", "$"+subTotal);
			}
			else{
				map.put("SubTotal", "$"+od.getUnitPrice());
			}
			listProd.add(map);
			
		}
		return listProd;
	}

}
