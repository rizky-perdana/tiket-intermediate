package com.tiket.spring.service;

import com.tiket.spring.entity.OrderDetails;
import com.tiket.spring.jpa.repository.OrderDetailRepository;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;


public class PaymentDetailService {
	@Autowired
	private OrderDetailRepository orderDetailRepository;
	
	public List<Object> getDetailPaymentQuery(long orderID) {
		List<OrderDetails> odlist = orderDetailRepository.findAll();
		List<Double> objCount = new ArrayList<>();
		List<Object> obj = new ArrayList<>();
		Map<String, Object> map = new HashMap<>();
		List<OrderDetails> tempList = new ArrayList<>();
                
                odlist.stream().filter((orderDetail) -> (orderDetail.getOrder().getOrderID() == orderID)).forEachOrdered((orderDetail) -> {
                    OrderDetails od = new OrderDetails();
                    od.setQuantity(orderDetail.getQuantity());
                    od.setUnitPrice(orderDetail.getUnitPrice());
                    od.setDiscount(orderDetail.getDiscount());
                    map.put("Taxes", orderDetail.getOrder().getTaxes());
                    map.put("Shipping & handling", orderDetail.getOrder().getFreightCharge());
                    tempList.add(od);
            });
		double subtotal;
		for (int i = 0; i < tempList.size(); i++) {
			double price = tempList.get(i).getUnitPrice();
			Long qty = tempList.get(i).getQuantity();
			double discount = tempList.get(i).getDiscount();
			if (discount != 0) {
				subtotal = (price * qty) - ((price * qty * discount) / 100);
			} else {
				subtotal = (price * qty);
			}
			objCount.add(subtotal);

		}
		double total = objCount.stream().mapToInt(Double::intValue).sum();
		map.put("Order Subtotal", total);
		double ot = total + (Long) map.get("Taxes") + (Long) map.get("Shipping & handling");
		map.put("Order Total", ot);

		obj.add(map);

		return obj;
	}
}
