package com.tiket.spring.service;

import com.tiket.spring.entity.Orders;
import com.tiket.spring.jpa.repository.OrderRepository;
import com.tiket.spring.jpa.repository.OrderRepository.ProductDetail;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl extends PaymentDetailService implements OrderService {
	
	@Autowired
	private OrderRepository orderRepository;

	@Override
	public void uploadOrder(List<Orders> orders) {
		for (Orders order : orders) {
			orderRepository.save(order);
		}
		
	}

	@Override
	public Orders findOrder(Long orderID) {
		Orders od = orderRepository.findById(orderID).get();
		return od;
	}

	@Override
	public List<ProductDetail> findProductDetail(Long orderID) {
		return orderRepository.findProductDetail(orderID);
	}


	@Override
	public List<Object> payDetail(Long orderID) {
		List<Object> obj = getDetailPaymentQuery(orderID);
		return obj;
	}

}
