package com.tiket.spring.service;

import com.tiket.spring.entity.OrderDetails;
import java.util.List;

public interface OrderDetailService {
	public void uploadOrderDetail(List<OrderDetails> orderDetail);
}
