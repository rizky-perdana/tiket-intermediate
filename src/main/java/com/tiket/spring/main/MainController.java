package com.tiket.spring.main;

import java.text.ParseException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tiket.spring.entity.Customers;
import com.tiket.spring.entity.Employees;
import com.tiket.spring.entity.OrderDetails;
import com.tiket.spring.entity.Orders;
import com.tiket.spring.entity.Products;
import com.tiket.spring.entity.ShippingMethods;
import com.tiket.spring.jpa.repository.OrderRepository.ProductDetail;
import com.tiket.spring.service.CustomerService;
import com.tiket.spring.service.EmployeeService;
import com.tiket.spring.service.OrderDetailService;
import com.tiket.spring.service.OrderService;
import com.tiket.spring.service.ProductService;
import com.tiket.spring.service.ShippingMethodService;
import com.tiket.spring.util.CsvDataLoader;
import com.tiket.spring.util.CsvPath;

@RestController
@RequestMapping("/rest")
public class MainController {

    @Autowired
    private CsvDataLoader loader;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private EmployeeService empService;

    @Autowired
    private ProductService prodService;

    @Autowired
    private ShippingMethodService shippingMethodService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderDetailService odService;

    @GetMapping("/dataPemesan")
    public Page<Customers> getAllCustomer(@PageableDefault(size = 5) Pageable pageable) {
        List<Customers> obj = customerService.findCustomers();
        int start = (int) pageable.getOffset();
        int end = (start + pageable.getPageSize()) > obj.size() ? obj.size()
                : (start + pageable.getPageSize());
        return new PageImpl<>(obj.subList(start, end), pageable, obj.size());
    }

    @GetMapping("/dataKaryawanByOrder")
    public PageImpl<Object> getEmployeeByOrder(@PageableDefault(size = 5) Pageable pageable) {
        List<Object> obj = empService.findEmployeeByOrder();
        int start = (int) pageable.getOffset();
        int end = (start + pageable.getPageSize()) > obj.size() ? obj.size()
                : (start + pageable.getPageSize());
        return new PageImpl<>(obj.subList(start, end), pageable, obj.size());
    }

    @GetMapping("/shippingMethodByOrder")
    public PageImpl<Object> getSmInUsed(@PageableDefault(size = 5) Pageable pageable) {
        List<Object> obj = shippingMethodService.findShippingMethodByOrder();
        int start = (int) pageable.getOffset();
        int end = (start + pageable.getPageSize()) > obj.size() ? obj.size()
                : (start + pageable.getPageSize());
        return new PageImpl<>(obj.subList(start, end), pageable, obj.size());
    }

    @RequestMapping(value = "/getProductDetail/{orderId}", method = RequestMethod.GET)
    public Page<ProductDetail> getProductDetail(@PathVariable("orderId") long orderID,
            @PageableDefault(size = 5) Pageable pageable) {
        List<ProductDetail> prodDetail = orderService.findProductDetail(orderID);
        int start = (int) pageable.getOffset();
        int end = (start + pageable.getPageSize()) > prodDetail.size() ? prodDetail.size()
                : (start + pageable.getPageSize());
        return new PageImpl<>(prodDetail.subList(start, end), pageable, prodDetail.size());
    }

    @RequestMapping(value = "/getPaymentDetail/{orderId}", method = RequestMethod.GET)
    public List<Object> payDetail(@PathVariable("orderId") long orderID) {
        return orderService.payDetail(orderID);
    }

    @GetMapping("/uploadEmployeeCSV")
    public void uploadEmployeeCSV() {
        List<Employees> emp = loader.readCsvEmployees(CsvPath.EMPLOYEES);
        empService.uploadEmployee(emp);
    }

    @GetMapping("/uploadProductCSV")
    public void uploadProductCSV() {
        List<Products> prod = loader.readCsvProducts(CsvPath.PRODUCTS);
        prodService.uploadProducts(prod);
    }

    @GetMapping("/uploadShippingMethodCSV")
    public void uploadShippingMethodCSV() {
        List<ShippingMethods> sm = loader.readCsvShippingMethods(CsvPath.ShippingMethods);
        shippingMethodService.uploadShippingMethod(sm);
    }

    @GetMapping("/uploadCustomerCSV")
    public void uploadCustomerCSV() {
        List<Customers> customers = loader.readCsvCustomerMethods(CsvPath.CUSTOMERS);
        customerService.uploadCustomer(customers);
    }

    @GetMapping("/uploadOrderCSV")
    public void uploadOrderCSV() throws ParseException {
        List<Orders> order = loader.readCsvOrder(CsvPath.ORDERS);
        orderService.uploadOrder(order);
    }

    @GetMapping("/uploadOrderDetailCSV")
    public void uploadOrderDetailCSV() {
        List<OrderDetails> od = loader.readCsvOrderDetail(CsvPath.ORDERDETAILS);
        odService.uploadOrderDetail(od);
    }

}
