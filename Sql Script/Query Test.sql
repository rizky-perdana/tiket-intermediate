1.
select customer.* from "Customers" customer inner join "Orders" o on o."CustomerID" = customer."CustomerID"
where customer."City" = 'Irvine';

2.
select c.* from "Customers" c 
inner join "Orders" o on o."CustomerID" = c."CustomerID"
inner join "Employees" e on e."EmployeeID" = o."EmployeeID"
where e."FirstName" = 'Adam' and e."LastName" = 'Barr';

3.
select p.* from "Products" p
inner join "Order_Details" od on od."ProductID" = p."ProductID"
inner join "Orders" o on o."OrderID" = od."OrderID"
inner join "Customers" cs on cs."CustomerID" = o."CustomerID"
where cs."CompanyName" like 'Contoso%'

4.
select o.* from "Orders" o 
inner join "Shipping_Methods" sm on sm."ShippingMethodID" = o."ShippingMethodID"
where sm."ShippingMethod" = 'UPS Ground'

5.
select 
sum( (od."UnitPrice" * od."Quantity") - ((od."UnitPrice" * od."Quantity" * od."Discount")/100 )) - o."Taxes" -
(CASE 
	WHEN o."FreightCharge" != null 
	THEN o."FreightCharge"
		ELSE 0
END )
as TotalOrder,
o."OrderID",
o."OrderDate"
from "Orders" o 
inner join "Order_Details" od on od."OrderID" = o."OrderID"
group by o."OrderID"
order by o."OrderDate" desc